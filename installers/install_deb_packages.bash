OS=`lsb_release -sc`

PACKAGES_18_04="libeigen3-dev \
 libsuitesparse-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libreadline-dev \
 qtdeclarative5-dev \
 qt5-qmake \
 libqglviewer-dev-qt5 \
 libudev-dev \
 freeglut3-dev \
 libgtest-dev \
 arduino \
 arduino-mk"

PACKAGES_16_04="libeigen3-dev \
 libsuitesparse-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libreadline-dev \
 libreadline6-dev \
 qtdeclarative5-dev \
 qt5-qmake \
 libqglviewer-dev \
 libudev-dev \
 libgtest-dev \
 freeglut3-dev \
 arduino \
 arduino-mk\
 libglfw3-dev"

PACKAGES_14_04="libeigen3-dev \
 libsuitesparse-metis-dev \
 ninja-build \
 libncurses5-dev \
 libwebsockets-dev \
 libqt4-dev \
 qt4-qmake \
 libqglviewer-dev \
 libudev-dev \
 freeglut3-dev \
 arduino \
 arduino-mk"

PACKAGES=""
if [ "${OS}" = "trusty" ]; then
    echo "14.04 selected\n"
    PACKAGES=${PACKAGES_14_04};
elif [ "${OS}" = "xenial" ]; then
    echo "16.04 selected\n"
    PACKAGES=${PACKAGES_16_04};
elif [ "${OS}" = "bionic" ]; then
    echo "18.04 selected\n"
    PACKAGES=${PACKAGES_18_04};
else
    echo "unknown OS, aborting\n"
    return 0
fi;

echo ${PACKAGES}

# required libraries and packages
echo "Installing ubuntu packages"
sudo apt-get update
sudo apt-get install ${PACKAGES} -y
echo "done"
