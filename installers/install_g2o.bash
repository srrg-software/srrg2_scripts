source ../configs/config_global.bash

OS=`lsb_release -sc`

if [ "${OS}" = "trusty" ]; then
    echo "14.04 selected\n"
    REPO="https://github.com/mtlazaro/g2o.git"
    REVISION="1b118ac2ed2055c4016c3b7cbd710225ed1651af"
    #commit previous to Qt5
elif [ "${OS}" = "xenial" ]; then
    echo "16.04 selected\n"
    REPO="https://github.com/RainerKuemmerle/g2o.git"
    REVISION="4b9c2f5b68d14ad479457b18c5a2a0bce1541a90"
else 
    echo "unsuported OS version, you will do the configuration my hand, aborting"
    return 0
fi

## libraries
echo "Cloning g2o, repo: ${REPO} ${REVISION}"
cd ${SOURCE_LIBRARIES_DIR}
git clone ${REPO}
cd g2o
git checkout ${REVISION}


#Building
echo "Building g2o"
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_WITH_MARCH_NATIVE=ON
make -j4
cd ../g2o
ln -s ../build/g2o/config.h .
echo " Done"
