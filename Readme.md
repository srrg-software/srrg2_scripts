# srrg_scripts

This repository contains the Download and Build scripts for the srrg repo(s) 

Fully Supported: 
- Ubuntu 16.04, ROS Kinetic

Partially Supported (in `srrg_boss`: **git checkout 72f24f75** to compile, due to gcc < 4.9 ):
- Ubuntu 14.04, ROS Indigo 

## Prerequisites
one of the two above distributions with the corresponding
ros version installed.
If you use 14.04 and you have Qt5 installed you might have installation
issues

## Setup

Set-up your ssh keys following the instructions in:

[https://gitlab.com/help/ssh/README.md](https://gitlab.com/help/ssh/README.md)

To install the environment simply do 

```
source srrg_setup.sh <packages file>
```

the <packages file> can be either

* configs/packages_full.bash
* configs/packages_public.bash
* configs/packages_orazio.bash
* configs/packages_mapper2d.bash
* configs/packages_mapper2d_public.bash
* configs/packages_srrg2.bash

depending on what you want/can install. You can also make
your own packages list, as it contains a list
of git packages to download from srrg repos.

Run on a freshly installed machine with ros-desktop-full

### Maintenance instructions ###

When you add a new package in the srrg environment, please update the following files accordingly (in the configs directory):

   * packages_full.txt
   * packages_public.txt

or

   * packages_srrg2.txt
