#source config.bash
CURRENT_DIR=`pwd`

## srrg_packages
echo "Executing cloc on installed SRRG packages"

for package in ./*
do
  cd ${CURRENT_DIR}
  if [[ -d "$package" ]]
  then
    echo "FOLDER:  ${package}"
    cloc ${package}/*
    echo ""
  fi
done

echo " Done"
cd $CURRENT_DIR
