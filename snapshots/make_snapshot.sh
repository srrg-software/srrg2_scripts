TARGET=$1

mkdir -p ${TARGET}/src
while read p; do
    echo "$p"
    rsync -avrL --exclude=".git" src/"$p" ${TARGET}/src/"$p"
done < $2
    
