# DIRECTORY LAYOUT

if  ! [ -e "$1" ] ; then
  echo "$1 not found."
  return 0
elif ! [[ $1 == *.bash ]]; then
  echo "extension should be '.bash'"
  return 0
fi

source $1

OS=`lsb_release -sc`

if [ "${OS}" = "trusty" ]; then
    echo "14.04 selected"
elif [ "${OS}" = "xenial" ]; then
    echo "16.04 selected"
elif [ "${OS}" = "bionic" ]; then
    echo "18.04 selected"
else 
    echo "unsuported OS version, you will do the configuration my hand, aborting"
    return 0
fi

echo "CONFIGURATION: "
echo "   OS:       ${OS}"
echo "   NEEDS_G2O:            ${NEEDS_G2O}"
echo "   NEEDS_FREENECT:       ${NEEDS_FREENECT}"
echo "   NEEDS_OPENNI2:        ${NEEDS_OPENNI2}"
echo "   SOURCE_DIR:           ${SOURCE_DIR}"
echo "   FETCH_MODE:           ${FETCH_MODE}"
echo "   SOURCE_LIBRARIES_DIR: ${SOURCE_LIBRARIES_DIR}"
echo "   SOURCE_EXTERNAL_DIR:  ${SOURCE_EXTERNAL_DIR}"
echo "   SRRG_PACKAGES_LIST:   ${SRRG_PACKAGES_LIST}"
echo "   SRRG_WORKSPACE_PATH:  ${SRRG_WORKSPACE_PATH}"
echo "   SRRG_DOWNLOAD_PATH:   ${SRRG_DOWNLOAD_PATH}"
echo "   COMPILE_WORKSPACE:    ${COMPILE_WORKSPACE}"
echo "   BRANCH:               ${BRANCH}"
 

sleep 3
CURRENT_DIR=${PWD}
SCRIPTS_DIR=${PWD}
echo "Creating Directory Layout"
mkdir -p ${SOURCE_DIR}
mkdir -p ${SOURCE_LIBRARIES_DIR}
mkdir -p ${SOURCE_EXTERNAL_DIR}
mkdir -p ${SRRG_DOWNLOAD_PATH}

sleep 1
echo "fetching deb packages"
source ${CURRENT_DIR}/installers/install_deb_packages.bash    

cd ${CURRENT_DIR} ;
if [ "${NEEDS_G2O}" = "true" ]; then
    sleep 1
    source ${CURRENT_DIR}/installers/install_g2o.bash
fi

cd ${CURRENT_DIR} ;
if [ "${NEEDS_OPENNI2}" = "true" ]; then
    sleep 1
    source ${CURRENT_DIR}/installers/install_openni2.bash         
    cd ${CURRENT_DIR}
fi

cd ${CURRENT_DIR} ;
if [ "${NEEDS_FREENECT}" = "true" ]; then
    sleep 1
    source ${CURRENT_DIR}/installers/install_freenect.bash         
    cd ${CURRENT_DIR}
fi

echo "cloning srrg_packages"
sleep 1
PREFIX="";
if [ "${FETCH_MODE}" = "git" ]; then
    PREFIX="git@gitlab.com:srrg-software";
else
    PREFIX="https://gitlab.com/srrg-software";
fi;
  
## srrg_packages
cd ${CURRENT_DIR}
sleep 1
while read in; 
do 
    cd ${SRRG_DOWNLOAD_PATH}
    ARG="${PREFIX}/${in}"
    echo "git clone ${ARG}"
    git clone ${ARG}
    # move into cloned package and checkout required branch
    package_name="$(cut -d'.' -f1 <<<"$in")"
    cd ${package_name}
    echo "cd $package_name; git checkout ${BRANCH}"
    git checkout ${BRANCH}
    cd ${CURRENT_DIR}
done < ${SRRG_PACKAGES_LIST}
echo "Done"


# this will be the dir for our current workspace
echo "making symlinks"
cd ${CURRENT_DIR}
sleep 1
mkdir -p ${SRRG_WORKSPACE_PATH}
cd ${SRRG_WORKSPACE_PATH}
mkdir src
echo "Done"
cd ${SRRG_WORKSPACE_PATH}/src
ln -s ${SRRG_DOWNLOAD_PATH}/* .

echo "Done"

if [ "$COMPILE_WORKSPACE" = true ] ; then
    echo "building workspace"
    cd ${SRRG_WORKSPACE_PATH}/src
    catkin_init_workspace
    cd ..
    catkin build
    echo "Done"
fi

cd ${CURRENT_DIR}

